<?php
namespace Haskel\QueueEventsBundle\Publisher;

use Exception;
use ReflectionClass;
use Stomp;
use StompFrame;

use Haskel\QueueEventsBundle\Router\EventRouterInterface;
use Haskel\QueueEventsBundle\Event\Synchronous;
use Haskel\QueueEventsBundle\Message\MapMessage;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Stomp publisher that is used to deliver events to the message broker via the Stomp protocol.
 */
class StompEventPublisher extends AbstractBasePublisher implements ContainerAwareInterface
{

    /**
     * Version of event publisher
     */
    const EVENT_VERSION = '0.1';

    /**
     * Instance of event router
     *
     * @var null|EventRouterInterface
     */
    protected $router = null;

    /**
     * Use MapMessage instead of default StompFrame
     *
     * @var bool
     */
    protected $useMapping = false;

    /**
     * Additional options for Stomp protocol
     *
     * @var array
     */
    protected $options = array(
        'headers' => array(),
        'prefix'  => '/queue/'
    );

    /**
     * Instance of container
     *
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Identifier of stomp service.
     *
     * Crazy extension connects in constructor and can throw an exception in container
     *
     * @var string
     */
    protected $stompServiceId;

    /**
     * Instance of stomp or null
     *
     * @var null|Stomp|\FuseSource\Stomp\Stomp
     */
    protected $stomp = null;

    /**
     * Constructor for stomp publisher
     *
     * @param EventRouterInterface $router Router for messages
     * @param string $stompServiceId Identifier of stomp service
     * @param array $options Additional options
     */
    public function __construct(EventRouterInterface $router, $stompServiceId, array $options)
    {
        $this->router         = $router;
        $this->options        = $options + $this->options;
        $this->stompServiceId = $stompServiceId;

        $headers = $this->options['headers'];
        $this->useMapping = isset($headers['transformation']) && $headers['transformation'] === 'jms-map-json';
    }

    /**
     * Injecting of container is needed for handling STOMP exceptions in constructor
     *
     * @param ContainerInterface $container A ContainerInterface instance
     *
     * @api
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        try {
            $this->stomp = $container->get($this->stompServiceId);
            $this->stomp->connect();
        } catch (Exception $e) {
            $container->get('logger')->warn($e->getMessage());
            $this->stomp = null;
        }
    }

    /**
     * Publish event to the specific destination
     *
     * @param string $eventName Name of the event
     * @param Event $event Subscribed event
     *
     * @return mixed|void
     */
    public function publishEvent($eventName, Event $event)
    {
        $message = $this->convertEventToMessage($eventName, $event);
        $headers = $this->options['headers'];

        $destination = $this->router->getDestination($eventName, $event);

        // In case when there is no connection to the stomp
        if (!$this->stomp) {
            user_error('Can not send message to the queue server', E_WARNING);
            return;
        }

        try {
            if ($event instanceof Synchronous) {
                $tempQueue = '/temp-queue/' . $destination;
                $message->headers['reply-to'] = $tempQueue;
                $this->stomp->subscribe($tempQueue, $headers);
            }

            $this->stomp->send($this->options['prefix'] . $destination, $message->body, $message->headers);

            if ($event instanceof Synchronous) {
                $message = $this->stomp->readFrame();
                if ($message) {
                    $event->setResponse(json_decode($message->body, true));
                    $this->stomp->ack($message, $headers);
                    $this->stomp->unsubscribe($tempQueue, $headers);
                }
            }
        } catch (Exception $e) {
            user_error($e->getMessage(), E_WARNING);
        }
    }

    /**
     * Converts the event to the message for
     *
     * @param string $eventName Name of the dispatched event
     * @param Event $event Event instance
     *
     * @return StompFrame
     */
    protected function convertEventToMessage($eventName, Event $event)
    {
        static $mask = 2047; // PRIVATE+PROTECTED+PUBLIC

        $reflectionEvent = new ReflectionClass($event);
        $eventClass      = $reflectionEvent->getName();
        $eventHeaders    = array(
            'EventClass'   => $eventClass,
            'EventName'    => $eventName,
            'EventVersion' => static::EVENT_VERSION
        );

        $eventData = array();
        foreach ($reflectionEvent->getProperties($mask) as $property) {
            $property->setAccessible(true);
            $value = $property->getValue($event);

            $eventData[$property->name] = $value;
            // Copy all scalar values to the headers
            if (is_scalar($value)) {
                $eventHeaders["Event{$property->name}Property"] = $value;
            }
        }
        $eventData = array($eventClass=>$eventData);
        // for stomp extension
//        if ($this->useMapping) {
//            $message = new MapMessage(null, $eventHeaders, $eventData);
//        } else {
//            $message = new StompFrame(null, $eventHeaders, json_encode($eventData));
//        }

        // fuse stomp
        $message = new \stdClass();
        $message->headers = $eventHeaders;
        $message->body    = json_encode($eventData);

        return $message;
    }

}
