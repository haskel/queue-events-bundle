<?php
namespace Haskel\QueueEventsBundle\Publisher;

use Symfony\Component\EventDispatcher\Event;

/**
 * Publisher of events from the Symfony2 to the general queues via specific protocol.
 */
abstract class AbstractBasePublisher implements PublisherInterface
{

    /**
     * Publish event to the specific destination
     *
     * @param string $eventName Name of the event
     * @param Event $event Subscribed event
     *
     * @return mixed
     */
    abstract public function publishEvent($eventName, Event $event);

    /**
     * Magic event dispatcher
     *
     * @param string $name Callback name
     * @param array $arguments Arguments for callback, Event instance
     *
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        // this will convert onTestEvent to test.event
        $eventName = substr(strtolower(preg_replace('/(?<=[a-z])([A-Z])/', '.$1', $name)), 3);
        $event     = array_shift($arguments);

        return $this->publishEvent($eventName, $event);
    }
}
