<?php
namespace Haskel\QueueEventsBundle\Publisher;

use Symfony\Component\EventDispatcher\Event;

/**
 * Publisher interface
 */
interface PublisherInterface
{

    /**
     * Publish event to the specific destination
     *
     * @param string $eventName Name of the event
     * @param Event $event Subscribed event
     *
     * @return mixed
     */
    public function publishEvent($eventName, Event $event);
}
