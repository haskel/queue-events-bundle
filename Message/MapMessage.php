<?php
namespace Haskel\QueueEventsBundle\Message;

use StompFrame;

class MapMessage extends StompFrame
{
    /**
     * Static mappings of PHP=>JMS types
     *
     * @var array
     */
    protected static $typeMap = array(
        'integer' => 'long'
    );

    /**
     * Map data
     *
     * @var array
     */
    public $map = array();

    public function __construct($command = null, array $headers = null, $body = null)
    {
        if (is_array($body)) {
            $this->map = $body;
            $body      = json_encode(array('map' => $this->mapData($body)));
        } elseif (is_string($body)) {
            $this->map = $this->unmapData($body);
        }
        parent::__construct($command, $headers, $body);
    }

    /**
     * Map the data to the JMS type
     *
     * @param array $data
     *
     * @return array JMS-ready entry
     */
    protected function mapData(array $data)
    {
        $packet  = array();
        $keyType = 'string'; // always use string type for keys
        foreach ($data as $key => $value) {

            // Only primitive types can be encoded directly, objects and array will be json-encoded
            if (!is_scalar($value)) {
                $value = json_encode(array('json' => $value));
            }
            $valueType = gettype($value);
            if (isset(self::$typeMap[$valueType])) {
                $valueType = self::$typeMap[$valueType];
            }
            if ($keyType === $valueType) {
                $packet[] = array($keyType => array($key, $value));
            } else {
                $packet[] = array($keyType => $key, $valueType => $value);
            }
        }
        return array('entry' => $packet);
    }

    /**
     * Unmap the data from the JMS type
     *
     * @param string $data
     *
     * @return array decoded data
     */
    protected function unmapData($data)
    {
        $result = array();
        $packet = json_decode($data, true);
        foreach ($packet['map']['entry'] as $entry) {
            if (count($entry) == 1) {
                // {"string":["key","value"]}
                list ($key, $value) = reset($entry);
            } else {
                // {"string":"key","double":value}
                list ($key, $value) = array_values($entry);
            }
            // Special case for our internal json {"json" :
            if (is_string($value) && substr($value, 0, 7) === '{"json"') {
                $decodedValue = json_decode($value, true);
                $value = $decodedValue['json'];
            }
            $result[$key] = $value;
        }
        return $result;
    }

}
