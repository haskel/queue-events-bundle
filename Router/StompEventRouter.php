<?php
namespace Haskel\QueueEventsBundle\Router;

use Symfony\Component\EventDispatcher\Event;

/**
 * Stomp event router interface is used to choose correct destination for event
 */
class StompEventRouter implements EventRouterInterface
{
    /**
     * Identifier ot global type for namespacing
     */
    const MESSAGE_TYPE = 'event';

    /**
     * Name of the kernel
     *
     * @var string
     */
    protected $kernelName = '';

    /**
     * Stomp router constructor
     *
     * @param string $kernelName name of the current kernel
     */
    public function __construct($kernelName)
    {
        $this->kernelName = $kernelName;
    }

    /**
     * Returns the destination for an event
     *
     * @param string $eventName Name of the event
     * @param Event $event Event data
     *
     * @return string
     */
    public function getDestination($eventName, Event $event)
    {
        static $cache = array();

        if (!isset($cache[$eventName])) {
            $eventParts = explode('.', $eventName);
            array_unshift($eventParts, self::MESSAGE_TYPE, $this->kernelName);

            $eventParts = array_map('ucfirst', $eventParts);
            $cache[$eventName] = join('.', $eventParts);
        }

        return $cache[$eventName];
    }
}
