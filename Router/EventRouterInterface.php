<?php
namespace Haskel\QueueEventsBundle\Router;

use Symfony\Component\EventDispatcher\Event;

/**
 * Event router interface is used to choose correct destination for event
 */
interface EventRouterInterface
{

    /**
     * Returns the destination for an event
     *
     * @param string $eventName Name of the event
     * @param Event $event Event data
     *
     * @return string
     */
    public function getDestination($eventName, Event $event);
}
