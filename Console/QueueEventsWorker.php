<?php
namespace Haskel\QueueEventsBundle\Console;

use Haskel\CoreBundle\Console\Daemon\Task;
use Haskel\CoreBundle\Console\Daemon\AbstractWorker;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;

class QueueEventsWorker extends AbstractWorker
{
    /**
     * Returns next task to process or null if there isn't new task
     *
     * @param \Symfony\Component\HttpKernel\KernelInterface   $kernel Kernel instance
     * @param \Symfony\Component\Console\Input\InputInterface $input  Input interface
     *
     * @return Task|null
     */
    protected function getNextTask(KernelInterface $kernel, InputInterface $input)
    {
        /** @var $subscriber \Haskel\QueueEventsBundle\Subscriber\StompEventSubscriber */
        static $subscriber = null;
        if (!$subscriber) {
            $container      = $this->kernel->getContainer();
            $subscriberName = $input->getOption('subscriber');
            $subscriberType = $input->getOption('type');
            $serviceId      = sprintf("haskel.queue_events.%s.event_subscriber.%s", $subscriberType, $subscriberName);
            $subscriber     = $container->get($serviceId);
        }

        $subscriber->pollEvents();

        return null; // No task to process, just need to poll events
    }

    /**
     * {@inheritdoc}
     */
    protected function getDefinitions()
    {
        return array_merge(
            parent::getDefinitions(),
            array(
                new InputOption(
                    'subscriber',
                    null,
                    InputOption::VALUE_REQUIRED,
                    "Specifies the name of subscriber to run. List of subscribers are configured in the application"
                ),
                new InputOption(
                    'type',
                    null,
                    InputOption::VALUE_OPTIONAL,
                    "Specifies the type of subscriber in lower case",
                    'stomp'
                )
            )
        );
    }
}
