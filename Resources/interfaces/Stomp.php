<?php
/**
 * Represents a connection between PHP and a Stomp compliant Message Broker.
 *
 * @link http://www.php.net/manual/en/class.stomp.php
 */
class Stomp
{
    /**
     * Rolls back a transaction in progress
     *
     * @link http://www.php.net/manual/en/stomp.abort.php
     *
     * @param string $transactionId The transaction to abort.
     * @param array $headers Associative array containing the additional headers (example: receipt).
     *
     * @return bool Returns true on success or false on failure.
     */
    public function abort($transactionId, array $headers = null) {}

    /**
     * Acknowledges consumption of a message
     *
     * @link http://www.php.net/manual/en/stomp.ack.php
     *
     * @param mixed $msg The message/messageId to be acknowledged.
     * @param array $headers Associative array containing the additional headers (example: receipt).
     *
     * @return bool Returns true on success or false on failure.
     */
    public function ack($msg, array $headers = null) {}

    /**
     * Starts a transaction
     *
     * @link http://www.php.net/manual/en/stomp.begin.php
     *
     * @param string $transactionId The transaction id.
     * @param array $headers Associative array containing the additional headers (example: receipt).
     *
     * @return bool Returns true on success or false on failure.
     */
    public function begin($transactionId, array $headers = null) {}

    /**
     * Commits a transaction in progress
     *
     * @link http://www.php.net/manual/en/stomp.commit.php
     *
     * @param string $transactionId The transaction id.
     * @param array $headers Associative array containing the additional headers (example: receipt).
     *
     * @return bool Returns true on success or false on failure.
     */
     public function commit($transactionId, array $headers = null) {}

    /**
     * Opens a connection
     *
     * @link http://www.php.net/manual/en/stomp.construct.php
     *
     * @param string $broker The broker URI
     * @param string $username The username.
     * @param string $password The password.
     * @param array $headers Associative array containing the additional headers (example: receipt).
     */
    public function __construct($broker = '', $username = '', $password = '', array $headers = null) {}

    /**
     * Closes stomp connection
     *
     * @link http://www.php.net/manual/en/stomp.destruct.php
     *
     * @return bool Returns true on success or false on failure.
     */
    public function __destruct() {}

    /**
     * Gets the last stomp error
     *
     * @link http://www.php.net/manual/en/stomp.error.php
     *
     * @return string Returns an error string or false if no error occurred.
     */
    public function error() {}

    /**
     * Gets read timeout
     *
     * @link http://www.php.net/manual/en/stomp.getreadtimeout.php
     *
     * @return array Returns an array with 2 elements: sec and usec.
     */
    public function getReadTimeout() {}

    /**
     * Gets the current stomp session ID
     *
     * @link http://www.php.net/manual/en/stomp.getsessionid.php
     *
     * @retutn string session id on success or false on failure.
     */
    public function getSessionId() {}

    /**
     * Indicates whether or not there is a frame ready to read
     *
     * @link http://www.php.net/manual/en/stomp.hasframe.php
     *
     * @return bool Returns true if a frame is ready to read, or false otherwise.
     */
    public function hasFrame() {}

    /**
     * Reads the next frame
     *
     * @link http://www.php.net/manual/en/stomp.readframe.php
     *
     * @param string $className The name of the class to instantiate. If not specified, a stompFrame object is returned.
     *
     * @return StompFrame
     */
    public function readFrame($className = '') {}

    /**
     * Sends a message
     *
     * @link http://www.php.net/manual/en/stomp.send.php
     *
     * @param string $destination Where to send the message
     * @param mixed $msg  Message to send.
     * @param array $headers Associative array containing the additional headers (example: receipt).
     *
     * @return bool Returns true on success or false on failure.
     */
    public function send($destination, $msg, array $headers = null) {}

    /**
     * Sets read timeout
     *
     * @link http://www.php.net/manual/en/stomp.setreadtimeout.php
     *
     * @param integer $seconds The seconds part of the timeout to be set.
     * @param integer $microseconds The microseconds part of the timeout to be set.
     *
     * @return void
     */
    public function setReadTimeout($seconds, $microseconds = 0) {}

    /**
     * Registers to listen to a given destination.
     *
     * @link http://www.php.net/manual/en/stomp.subscribe.php
     *
     * @param string $destination Destination to subscribe to.
     * @param array $headers Associative array containing the additional headers (example: receipt).
     *
     * @return bool Returns true on success or false on failure.
     */
    public function subscribe($destination, array $headers = null) {}

    /**
     * Removes an existing subscription
     *
     * @link http://www.php.net/manual/en/stomp.unsubscribe.php
     *
     * @param string $destination Subscription to remove.
     * @param array $headers Associative array containing the additional headers (example: receipt).
     *
     * @return bool Returns true on success or false on failure.
     */
    public function unsubscribe($destination, array $headers = null) {}
}
