<?php

/**
 * Represents a message which was sent or received from a Stomp compliant Message Broker.
 */
class StompFrame
{
    /**
     * Frame command
     *
     * @var string
     */
    public $command = null;

    /**
     * Frame headers
     *
     * @var array
     */
    public $headers = null;

    /**
     * Frame body
     *
     * @var string
     */
    public $body = null;

    /**
     * Constructor.
     *
     * @param string $command Frame command
     * @param array $headers Frame headers
     * @param string $body Frame body
     */
    public function __construct($command = null, array $headers = null, $body = null) {}
}
