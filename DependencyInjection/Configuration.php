<?php
namespace Haskel\QueueEventsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{

    /**
     * Default status of module
     *
     * @var boolean
     */
    protected $enabled = true;

    /**
     * Constructs a configuration instance
     *
     * @param bool $standalone Core standalone mode
     */
    public function __construct($standalone = false)
    {
        $this->enabled = !$standalone;
    }

    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('queue_events');

        $rootNode
            ->children()
                ->booleanNode('enabled')->defaultValue($this->enabled)->end()
            ->end()
        ->end();

        $this->addStompPublisher($rootNode);
        $this->addStompSubscribers($rootNode);

        return $treeBuilder;
    }

    /**
     * Add definition for publishers and subscribers
     *
     * @param ArrayNodeDefinition $rootNode Node
     *
     * @return void
     */
    private function addStompPublisher(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->arrayNode('stomp_publisher')
                    ->children()
                        ->arrayNode('options')
                            ->useAttributeAsKey('name')
                            ->prototype('variable')->end()
                        ->end()
                    ->end()
                    ->fixXmlConfig('event')
                    ->children()
                        ->arrayNode('events')
                            ->isRequired()
                            ->prototype('scalar')->end()
                        ->end()
                    ->end()
                    ->children()
                        ->arrayNode('connection')
                            ->prototype('variable')->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    /**
     * Add definition for publishers and subscribers
     *
     * @param ArrayNodeDefinition $rootNode Node
     *
     * @return void
     */
    private function addStompSubscribers(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->arrayNode('stomp_subscribers')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('application')->isRequired()->end()
                            ->scalarNode('prefix')->defaultValue('/queue/Event.')->end()
                        ->end()
                        ->fixXmlConfig('event')
                        ->children()
                            ->arrayNode('events')
                                ->isRequired()
                                ->requiresAtLeastOneElement()
                                ->cannotBeEmpty()
                                ->prototype('variable')->end()
                            ->end()
                        ->end()
                        ->children()
                            ->arrayNode('connection')
                                ->prototype('variable')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
