<?php
namespace Haskel\QueueEventsBundle\DependencyInjection;

use Haskel\QueueEventsBundle\Router\StompEventRouter;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

class QueueEventsExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        if (!empty($config['stomp_publisher'])) {
            $this->loadStompPublisher($config['stomp_publisher'], $container);
        }

        if (!empty($config['stomp_subscribers'])) {
            $this->loadStompSubscribers($config['stomp_subscribers'], $container);
        }
    }

    /**
     * Loads stomp publisher definition into the container
     *
     * @param array $config Publisher configuration
     * @param ContainerBuilder $container Instance of container builder
     *
     * @return void
     */
    private function loadStompPublisher(array $config, ContainerBuilder $container)
    {
        // Initialize separate service for Stomp
        $stompDefinition = new DefinitionDecorator('haskel.queue_events.stomp');
        $stompDefinition->setArguments($config['connection']);
//        $stompDefinition->addMethodCall($config['connection']);

        $publisherDefinition = new DefinitionDecorator('haskel.queue_events.stomp.event_publisher');
        $publisherDefinition->replaceArgument(1, 'queue_events.stomp.publisher.connection');
        $publisherDefinition->replaceArgument(2, $config['options']);
        foreach ($config['events'] as $eventName) {
            $publisherDefinition->addTag('kernel.event_listener', array(
                'event'  => $eventName,
                'method' => $this->getMethodName($eventName)
            ));
        }

        $container->addDefinitions(array(
            'queue_events.stomp.publisher.connection'    => $stompDefinition,
            'queue_events.stomp.event_publisher.default' => $publisherDefinition
        ));
    }

    /**
     * Loads subscribers definition to the container
     *
     * @param array $config Subscriber config
     * @param ContainerBuilder $container Instance of container
     *
     * @return void
     */
    private function loadStompSubscribers(array $config, ContainerBuilder $container)
    {

        $definitions = array();

        foreach ($config as $name => $subscriberConfig) {

            // Initialize separate service for Stomp
            $stompServiceId  = sprintf('haskel.queue_events.stomp.connection.%s', $name);
            $stompDefinition = new DefinitionDecorator('haskel.queue_events.stomp');
            $stompDefinition->setArguments($subscriberConfig['connection']);

            $subscriberServiceId  = sprintf('haskel.queue_events.stomp.event_subscriber.%s', $name);
            $subscriberDefinition = new DefinitionDecorator('haskel.queue_events.stomp.event_subscriber');
            $subscriberDefinition->replaceArgument(0, new Reference($stompServiceId));

            $sources = $this->getStompSources($subscriberConfig);
            foreach ($sources as $source) {
                $subscriberDefinition->addMethodCall('subscribe', array($source));
            }

            $definitions += array(
                $stompServiceId      => $stompDefinition,
                $subscriberServiceId => $subscriberDefinition
            );
        }

        $container->addDefinitions($definitions);
    }


    /**
     * Returns the method name from event name
     *
     * @param string $eventName Name of the event
     *
     * @return string Corresponding method name
     */
    private function getMethodName($eventName)
    {
        $parts = explode('.', $eventName);
        $parts = array_map('ucfirst', $parts);
        return 'on' . join('', $parts);
    }

    /**
     * Returns stomp source for subscription
     *
     * @param array $subscriberConfig Configuration for specific subscriber
     *
     * @return array Stomp sources for connection
     */
    private function getStompSources($subscriberConfig)
    {
        $sources = array();
        foreach ($subscriberConfig['events'] as $event) {
            $parts = explode('.', $event);
            array_unshift($parts, StompEventRouter::MESSAGE_TYPE, $subscriberConfig['application']);
            $parts = array_map('ucfirst', $parts);

            $sources[] = $subscriberConfig['prefix'] . join('.', $parts);
        };
        return $sources;
    }
}
