<?php
namespace Haskel\QueueEventsBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\ParameterBag;

class RemoteEvent extends Event
{
    /**
     * Data from a remote event
     *
     * @var null|ParameterBag
     */
    public $data = null;

    /**
     * List of headers for the event
     *
     * @var null|ParameterBag
     */
    public $headers = null;

    /**
     * Remote event constructor
     *
     * @param array $data Data for the event
     * @param array $headers Additional list of headers
     */
    public function __construct(array $data = array(), array $headers = array())
    {
        if (isset($data['data'])) {
            $data = $data['data'];
        }
        $this->data    = new ParameterBag($data);
        $this->headers = new ParameterBag($headers);
    }
}
