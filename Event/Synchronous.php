<?php
namespace Haskel\QueueEventsBundle\Event;

/**
 * Interface for synchronous events
 */
interface Synchronous
{

    /**
     * Sets a response for synchronous event
     *
     * @param mixed $response
     *
     * @return void
     */
    public function setResponse($response);

    /**
     * Gets a response from synchronous event
     *
     * @return mixed
     */
    public function getResponse();
}
