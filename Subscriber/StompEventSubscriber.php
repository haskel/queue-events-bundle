<?php
namespace Haskel\QueueEventsBundle\Subscriber;

use FuseSource\Stomp\Exception\StompException;
use FuseSource\Stomp\Frame;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Stomp;
use StompFrame;
use Exception;
use Haskel\QueueEventsBundle\Event\Synchronous;
use Haskel\QueueEventsBundle\Event\RemoteEvent;
use Haskel\QueueEventsBundle\Message\MapMessage;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Stomp subscriber is used to deliver events from the message broker via the Stomp protocol.
 */
class StompEventSubscriber
{

    /**
     * Stomp instance
     *
     * @var \FuseSource\Stomp\Stomp
     */
    protected $subscriber;

    /**
     * Event dispatcher instance
     *
     * @var null|EventDispatcherInterface
     */
    protected $dispatcher = null;

    /**
     * @var LoggerInterface|Logger
     */
    protected $logger;

    /**
     * Additional options for Stomp protocol
     *
     * @var array
     */
    protected $options = array(
        'headers'    => array(),
        'frameClass' => 'Haskel\QueueEventsBundle\Message\MapMessage'
    );

    /**
     * Use MapMessage instead of default StompFrame
     *
     * @var bool
     */
    protected $useMapping = false;

    /**
     * Stomp subscriber constructor
     *
     * @param \FuseSource\Stomp\Stomp $stomp Instance of stomp connection
     * @param array $options Additional options for configuration
     * @param EventDispatcherInterface $dispatcher Instance of dispatcher
     */
    public function __construct($stomp,
                                array $options,
                                EventDispatcherInterface $dispatcher,
                                LoggerInterface $logger)
    {
        $this->subscriber = $stomp;
        $this->options    = $options + $this->options;
        $this->dispatcher = $dispatcher;
        $this->logger     = $logger;

        $this->subscriber->connect();

        $headers = $this->options['headers'];
        $this->useMapping = isset($headers['transformation']) && $headers['transformation'] === 'jms-map-json';
    }

    /**
     * Subscribes to the events from the broker
     *
     * @param string $source Source for subscribing
     *
     * @return bool
     */
    public function subscribe($source)
    {
        return $this->subscriber->subscribe($source, $this->options['headers']);
    }

    /**
     * Poll events from the stomp and dispatch them while available
     *
     * @return void
     */
    public function pollEvents()
    {
        try {
            while ($this->subscriber->hasFrameToRead()) {

                if ($this->useMapping) {
                    $message = $this->subscriber->readFrame($this->options['frameClass']);
                } else {
                    $message = $this->subscriber->readFrame();
                }
                $messageId = $message->headers['message-id'];
                $this->log(sprintf("received message-id#{$messageId}, worker pid: %d", posix_getpid()));

                if (!isset($message->headers['EventName'])) {
                    $this->subscriber->ack($messageId);
                }
                // Prefix the name to prevent collision with standard event names
                $eventName = 'stomp.' . $message->headers['EventName'];
                $event = $this->convertMessageToEvent($message);
                try {
                    $this->dispatcher->dispatch($eventName, $event);
                } catch (Exception $e) {
                    $this->log($e->getMessage(), [$e->getTraceAsString()], Logger::ERROR);
                    $this->subscriber->ack($messageId);
                    $this->log(sprintf("failed: message-id#{$messageId}, worker pid: %d", posix_getpid()));
                    continue;
                }

                if ($event instanceof Synchronous && !empty($message->headers['reply-to'])) {
                    $response = new Frame(
                        null,
                        array(
                            'correlation-id' => $messageId
                        ),
                        json_encode($event->getResponse())
                    );
                    $this->subscriber->send($message->headers['reply-to'], $response);
                }

                // acknowledge for the message
                $this->subscriber->ack($messageId);
                $this->log(sprintf("success: message-id#{$messageId}, worker pid: %d", posix_getpid()));
            };
        } catch (StompException $e) {
            
        }
    }

    /**
     * @param string $message
     * @param array  $context
     * @param int    $level
     */
    private function log($message, $context = [], $level = Logger::ALERT)
    {
        $this->logger->withName('queue')->addRecord($level, $message, $context);
    }

    /**
     * Converts the message from broker to the Event
     * @todo: add class mapping & deserialization
     *
     * @param \FuseSource\Stomp\Frame $message Message from Stomp
     *
     * @return RemoteEvent
     */
    protected function convertMessageToEvent($message)
    {
        if ($message instanceof MapMessage) {
            $data = $message->map;
        } else {
            $data = json_decode($message->body, true);
        }

        // Extract a message data from data
        $messageData = reset($data);

        return new RemoteEvent($messageData, $message->headers);
    }
}
